//
//  ZLTableViewCell.h
//  ZLCell
//
//  Created by zhaolei on 2018/10/29.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZLTableViewCell : UITableViewCell

///头像
@property (nonatomic,weak) UIImageView *iconImageView;
///标题
@property (nonatomic,weak) UILabel *titleLabel;
///子标题
@property (nonatomic,weak) UILabel *subTitleLabel;

@end
