//
//  ViewController.m
//  ZLCell
//
//  Created by zhaolei on 2018/10/29.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ViewController.h"
#import <UIImageView+AFNetworking.h>
#import "ZLTableViewCell.h"

@interface ViewController ()<UITableViewDataSource>

///滑动视图
@property (nonatomic,weak) UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self tableView];
}

#pragma mark - Lazy
- (UITableView *)tableView {
    if (!_tableView) {
        UITableView *tableView = [[UITableView alloc] initWithFrame:UIScreen.mainScreen.bounds style:(UITableViewStylePlain)];
        tableView.rowHeight = 100.0;
        
        //ios11 适配
        if (@available(iOS 11.0, *)) {
            tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            tableView.scrollIndicatorInsets = tableView.contentInset;
            tableView.estimatedRowHeight = 0;
            tableView.estimatedSectionHeaderHeight = 0;
            tableView.estimatedSectionFooterHeight = 0;
        }
        
        tableView.dataSource = self;
        [self.view addSubview:tableView];
        _tableView = tableView;
    }
    return _tableView;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 12;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZLTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[ZLTableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    
//    cell.contentView.backgroundColor = [UIColor colorWithRed:arc4random()%255 / 255.0 green:arc4random()%255 / 255.0 blue:arc4random()%255 / 255.0 alpha:1.0];
    
    //为控件赋值
    [cell.iconImageView setImageWithURL:[NSURL URLWithString:@"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1540825221432&di=7fadc6fea4eddd71c2042df3e533f830&imgtype=0&src=http%3A%2F%2Fimg18.3lian.com%2Fd%2Ffile%2F201710%2F26%2Fdaabbdcf5727480ced3f6a97bd75e027.jpg"]];
    cell.titleLabel.text = @"title";
    cell.subTitleLabel.text = @"subTitle";
    
    if (indexPath.row == 3) {
        cell.subTitleLabel.textColor = UIColor.redColor;
    }else {
        cell.subTitleLabel.textColor = UIColor.blackColor;
    }
    
    return cell;
}

@end
