//
//  ZLTableViewCell.m
//  ZLCell
//
//  Created by zhaolei on 2018/10/29.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ZLTableViewCell.h"

@implementation ZLTableViewCell

#pragma mark - Lazy
- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        //头像
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15.0, 15.0, 100.0, 70.0)];
        //        imageView.backgroundColor = [UIColor colorWithRed:arc4random()%255 / 255.0 green:arc4random()%255 / 255.0 blue:arc4random()%255 / 255.0 alpha:1.0];
        [self.contentView addSubview:imageView];
        _iconImageView = imageView;
    }
    return _iconImageView;
}
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        //标题
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + 10.0, 15.0, UIScreen.mainScreen.bounds.size.width - CGRectGetMaxX(self.iconImageView.frame) - 25.0, 20.0)];
        //        label.backgroundColor = [UIColor colorWithRed:arc4random()%255 / 255.0 green:arc4random()%255 / 255.0 blue:arc4random()%255 / 255.0 alpha:1.0];
        [self.contentView addSubview:label];
        _titleLabel = label;
    }
    return _titleLabel;
}
- (UILabel *)subTitleLabel {
    if (!_subTitleLabel) {
        //子标题
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + 10.0, 65.0, UIScreen.mainScreen.bounds.size.width - CGRectGetMaxX(self.iconImageView.frame) - 25.0, 20.0)];
        //        label.backgroundColor = [UIColor colorWithRed:arc4random()%255 / 255.0 green:arc4random()%255 / 255.0 blue:arc4random()%255 / 255.0 alpha:1.0];
        [self.contentView addSubview:label];
        _subTitleLabel = label;
    }
    return _subTitleLabel;
}

@end
