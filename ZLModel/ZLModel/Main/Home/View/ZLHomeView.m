//
//  ZLHomeView.m
//  ZLModel
//
//  Created by zhaolei on 2018/10/30.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ZLHomeView.h"
#import "ZLHomeModel.h"
#import <UIImageView+AFNetworking.h>

@interface ZLHomeView ()<UITableViewDataSource,UITableViewDelegate>

///滑动视图
@property (nonatomic,weak) UITableView *tableView;

@end

@implementation ZLHomeView

#pragma mark - Set
- (void)setInfoModel:(ZLHomeModel *)infoModel {
    _infoModel = infoModel;
    [self registerAction];
}

#pragma mark - Lazy
- (UITableView *)tableView {
    if (!_tableView) {
        UITableView *tableView = [[UITableView alloc] initWithFrame:self.bounds style:(UITableViewStylePlain)];
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.rowHeight = 100.0;
        
        //ios11 适配
        if (@available(iOS 11.0, *)) {
            tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            tableView.scrollIndicatorInsets = tableView.contentInset;
            tableView.estimatedRowHeight = 0;
            tableView.estimatedSectionHeaderHeight = 0;
            tableView.estimatedSectionFooterHeight = 0;
        }
        
        [self addSubview:tableView];
        _tableView = tableView;
    }
    return _tableView;
}

#pragma mark - Action
//在得到模型的同时，注册模型的事件
- (void)registerAction {
    
    __weak typeof(self)weakSelf = self;
    
    //处理结果
    self.infoModel.result = ^{
        [weakSelf.tableView reloadData];
    };
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.infoModel.unitModelsM.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleSubtitle) reuseIdentifier:@"cell"];
        //图片
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15.0, 15.0, 70.0, 70.0)];
        imageView.tag = 5;
        [cell.contentView addSubview:imageView];
        //标题
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame) + 10.0, CGRectGetMinY(imageView.frame), UIScreen.mainScreen.bounds.size.width - CGRectGetMaxX(imageView.frame) - 60.0, 20.0)];
        label.tag = 6;
        [cell.contentView addSubview:label];
        //子标题
        label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame) + 10.0, CGRectGetMaxY(imageView.frame) - 20.0, UIScreen.mainScreen.bounds.size.width - CGRectGetMaxX(imageView.frame) - 60.0, 20.0)];
        label.textColor = UIColor.lightGrayColor;
        label.tag = 7;
        [cell.contentView addSubview:label];
        //对勾
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(UIScreen.mainScreen.bounds.size.width - 50.0, 0, 50.0, 100.0)];
        button.userInteractionEnabled = NO;
        button.tag = 8;
        [cell.contentView addSubview:button];
    }
    //找到控件
    UIImageView *imageView = [cell.contentView viewWithTag:5];
    UILabel *titleLabel = [cell.contentView viewWithTag:6];
    UILabel *subTitleLabel = [cell.contentView viewWithTag:7];
    UIButton *markButton = [cell.contentView viewWithTag:8];
    //恢复默认
    [markButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    //取出模型
    ZLHomeModel *unitModel = self.infoModel.unitModelsM[indexPath.row];
    //进行赋值
    [imageView setImageWithURL:[NSURL URLWithString:unitModel.urlStirng]];
    titleLabel.text = unitModel.title;
    subTitleLabel.text = unitModel.subTitle;
    //将与模型标记的索引值相同的单元格进行标记
    if (self.infoModel.indexPath.row == indexPath.row && self.infoModel.indexPath) {
        [markButton setImage:[UIImage imageNamed:@"勾选地区"] forState:UIControlStateNormal];
    }
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.infoModel.indexPath = indexPath;
    [tableView reloadData];
}
///是否可以进入编辑状态
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}
///为编辑状态下的单元格设置样式
- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self)weakSelf = self;
    UITableViewRowAction *action1 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPaths) {
        //找到对应的模型
        ZLHomeModel *unitModel = weakSelf.infoModel.unitModelsM[indexPaths.row];
        if (unitModel.deleteRow) {
            
            //调用删除单元格的block之前要先注册删除结果的block,以便结果回来时调用
            unitModel.deleteRowResult = ^{
                //刷新界面数据
                [weakSelf.infoModel.unitModelsM removeObjectAtIndex:indexPaths.row];
                weakSelf.infoModel.indexPath = nil;
                [weakSelf.tableView reloadData];
            };
            
            //删除后台数据
            unitModel.deleteRow();
        }
        
        tableView.editing = NO;
    }];
    action1.backgroundColor = [UIColor colorWithRed:1.0 green:55 / 255.0 blue:55 / 255.0 alpha:1.0];
    return @[action1];
}

@end
