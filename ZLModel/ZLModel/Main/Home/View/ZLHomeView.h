//
//  ZLHomeView.h
//  ZLModel
//
//  Created by zhaolei on 2018/10/30.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZLHomeModel;
@interface ZLHomeView : UIView

///强引用模型
@property (nonatomic,strong) ZLHomeModel *infoModel;

@end
