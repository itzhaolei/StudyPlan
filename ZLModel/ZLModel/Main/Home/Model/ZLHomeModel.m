//
//  ZLHomeModel.m
//  ZLModel
//
//  Created by zhaolei on 2018/10/30.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ZLHomeModel.h"
#import "ZLHTTPSessionManager.h"

@implementation ZLHomeModel

///加载模型
+ (instancetype)loadModel {
    ZLHomeModel *infoModel = [ZLHomeModel new];
    
    //避免循环引用、使用弱指针模型来操作 block
    __weak typeof(infoModel)weakModel = infoModel;
    
    //实现、注册事件
    infoModel.load = ^{
        [weakModel requestListData];
    };
    
    //执行、调用
    infoModel.load();
    
    return infoModel;
}

///删除后台数据
- (void)deleteRowAction {
    NSMutableDictionary *dictM = [NSMutableDictionary new];
    dictM[@"id"] = self.keyId;
    dictM[@"userid"] = @(9);
    dictM[@"token"] = @"$2y$10$GELXMlW4YrxdY0BpeCFHKesfD6xpH\\/XisAp81mmLuAmlZpU0dvG0i";
    __weak typeof(self)weakSelf = self;
    [ZLHTTPSessionManager requestDataWithUrlPath:@"/appmyhome/del_bank_card.go" Params:dictM POST:YES ModelArray:nil HttpHeader:NO CachePolicy:NO Results:^(ZLSessionManagerErrorState sessionErrorState, id responseObject) {
        if (!sessionErrorState) {
            //开始解析
            if (weakSelf.deleteRowResult) {
                weakSelf.deleteRowResult();
            }
            return;
        }
    }];
}

///请求列表数据
- (void)requestListData {
    NSMutableDictionary *dictM = [NSMutableDictionary new];
    dictM[@"p"] = @(1);
    dictM[@"rows"] = @(10);
    dictM[@"userid"] = @(9);
    dictM[@"token"] = @"$2y$10$GELXMlW4YrxdY0BpeCFHKesfD6xpH\\/XisAp81mmLuAmlZpU0dvG0i";
    __weak typeof(self)weakSelf = self;
    [ZLHTTPSessionManager requestDataWithUrlPath:@"/appmyhome/bank_card_list.go" Params:dictM POST:YES ModelArray:nil HttpHeader:NO CachePolicy:NO Results:^(ZLSessionManagerErrorState sessionErrorState, id responseObject) {
        if (!sessionErrorState) {
            //开始解析
            [weakSelf modelWithDictionary:responseObject];
            if (weakSelf.result) {
                weakSelf.result();
            }
            return;
        }
    }];
}

- (void)modelWithDictionary:(NSDictionary *)responseObject {
    NSDictionary *dataDict = responseObject[@"data"];
    if (![dataDict isKindOfClass:[NSDictionary class]]) {
        return;
    }
    if (!dataDict.count) {
        return;
    }
    NSArray *dataArray = dataDict[@"info"];
    if (![dataArray isKindOfClass:[NSArray class]]) {
        return;
    }
    if (!dataArray.count) {
        return;
    }
    
    //准备容器
    NSMutableArray *unitModelsM = [NSMutableArray new];
    
    for (NSInteger index = 0; index < dataArray.count; index++) {
        NSDictionary *unitDict = dataArray[index];
        ZLHomeModel *unitModel = [[ZLHomeModel alloc] init];
        unitModel.urlStirng = UIScreen.mainScreen.scale == 3 ? unitDict[@"img_3"] : unitDict[@"img_2"];
        unitModel.title = unitDict[@"bank_name"];
        unitModel.keyId = unitDict[@"id"];
        unitModel.subTitle = [NSString stringWithFormat:@"%@ %@",unitDict[@"bank_card"],unitDict[@"type"]];
        
        __weak typeof(unitModel)weakModel = unitModel;
        unitModel.deleteRow = ^{
            [weakModel deleteRowAction];
        };
        
        [unitModelsM addObject:unitModel];
    }
    
    self.unitModelsM = unitModelsM;
}

@end
