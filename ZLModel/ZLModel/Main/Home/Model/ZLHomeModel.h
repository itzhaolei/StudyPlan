//
//  ZLHomeModel.h
//  ZLModel
//
//  Created by zhaolei on 2018/10/30.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZLHomeModel : NSObject

///选中的索引
@property (nonatomic,strong) NSIndexPath *indexPath;

///主键id
@property (nonatomic,strong) NSString *keyId;
///图片路径
@property (nonatomic,strong) NSString *urlStirng;
///标题
@property (nonatomic,strong) NSString *title;
///子标题
@property (nonatomic,strong) NSString *subTitle;

///单元模型
@property (nonatomic,strong) NSMutableArray <ZLHomeModel *>*unitModelsM;

///加载数据
@property (nonatomic,copy) void (^load)(void);
///处理结果
@property (nonatomic,copy) void (^result)(void);
///删除单元格
@property (nonatomic,copy) void (^deleteRow)(void);
///删除单元格
@property (nonatomic,copy) void (^deleteRowResult)(void);

///加载模型
+ (instancetype)loadModel;

@end
