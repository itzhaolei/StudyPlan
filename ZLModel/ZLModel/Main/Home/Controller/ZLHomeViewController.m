
//
//  ZLHomeViewController.m
//  ZLModel
//
//  Created by zhaolei on 2018/10/30.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ZLHomeViewController.h"
#import "ZLHomeView.h"
#import "ZLHomeModel.h"

@interface ZLHomeViewController ()

///主视图
@property (nonatomic,weak) ZLHomeView *mainView;

@end

@implementation ZLHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mainView.infoModel = [ZLHomeModel loadModel];
}

#pragma mark - Lazy
//懒加载主视图，使控制器上只出现一个主视图，界面有关的视图都放在这个主视图上。
- (ZLHomeView *)mainView {
    if (!_mainView) {
        ZLHomeView *view = [[ZLHomeView alloc] initWithFrame:self.view.bounds];
        view.backgroundColor = [UIColor colorWithRed:arc4random()%255 / 255.0 green:arc4random()%255 / 255.0 blue:arc4random()%255 / 255.0 alpha:1.0];
        [self.view addSubview:view];
        _mainView = view;
    }
    return _mainView;
}

@end
