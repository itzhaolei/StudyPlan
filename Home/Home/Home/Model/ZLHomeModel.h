//
//  ZLHomeModel.h
//  Home
//
//  Created by zhaolei on 2018/11/6.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZLHomeModel : NSObject

///页码
@property (nonatomic,unsafe_unretained) NSInteger page;
///单页数量
@property (nonatomic,unsafe_unretained) NSInteger size;
///加载更多
@property (nonatomic,unsafe_unretained) BOOL loadMore;
///没有更多
@property (nonatomic,unsafe_unretained) BOOL noMore;

///主键id
@property (nonatomic,strong) NSString *keyId;
///标题
@property (nonatomic,strong) NSString *keyTitle;
///封面图地址
@property (nonatomic,strong) NSString *urlString;

///轮播封面图地址
@property (nonatomic,strong) NSArray <NSString *>*banners;
///轮播模型
@property (nonatomic,strong) NSArray <ZLHomeModel *>*bannerModels;
///单元模型
@property (nonatomic,strong) NSMutableArray <ZLHomeModel *>*unitModelsM;

///加载数据
@property (nonatomic,copy) void (^load)(void);
///处理结果
@property (nonatomic,copy) void (^result)(void);
///点击事件
@property (nonatomic,copy) void (^click)(NSInteger index);

///加载模型
+ (instancetype)loadModel;

@end

NS_ASSUME_NONNULL_END
