//
//  ZLHomeModel.m
//  Home
//
//  Created by zhaolei on 2018/11/6.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ZLHomeModel.h"
#import "ZLHTTPSessionManager.h"

@implementation ZLHomeModel

+ (instancetype)loadModel {
    ZLHomeModel *infoModel = [self new];
    //配置初始分页值
    infoModel.page = 1;
    infoModel.size = 10;
    
    __weak typeof(infoModel)weakModel = infoModel;
    
    infoModel.load = ^{
        [weakModel requestHomeData];
    };
    
    infoModel.load();
    
    return infoModel;
}

///加载首页数据
- (void)requestHomeData {
    
    __weak typeof(self)weakSelf = self;
    
    NSMutableDictionary *dictM = [NSMutableDictionary new];
    dictM[@"p"] = @(self.page);
    dictM[@"rows"] = @(self.size);
    dictM[@"cityid"] = @(273);
    [ZLHTTPSessionManager requestDataWithUrlPath:@"https://api.91tumi.com/apphome/index.go" Params:dictM POST:YES ModelArray:nil HttpHeader:NO CachePolicy:NO Results:^(ZLSessionManagerErrorState sessionErrorState, id responseObject) {
        if (!sessionErrorState) {
            if (![responseObject[@"code"] integerValue]) {//请求成功
                [weakSelf modelWithDictionary:responseObject];
                if (weakSelf.result) {
                    weakSelf.result();
                }
                return;
            }
            //后台有错误提示
            
            return;
        }
        //网络问题报错
        
    }];
}

///模型解析
- (void)modelWithDictionary:(NSDictionary *)responseObject {
    NSDictionary *dataDict = responseObject[@"data"];
    if (![dataDict isKindOfClass:[NSDictionary class]]) {
        return;
    }
    if (!dataDict.count) {
        return;
    }
    
    //因为轮播解析的频率不用高频，所以只有下拉刷新的时候进行解析就可以了
    if (!self.loadMore) {
        NSArray *banner = dataDict[@"banner"];
        if ([banner isKindOfClass:[NSArray class]]) {
            if (banner.count) {
                NSMutableArray *bannersM = [NSMutableArray new];
                NSMutableArray *bannerModelsM = [NSMutableArray new];
                for (NSInteger index = 0; index < banner.count; index++) {
                    NSDictionary *bannerDict = banner[index];
                    ZLHomeModel *unitModel = [[ZLHomeModel alloc] init];
                    unitModel.keyId = bannerDict[@"adid"];
                    unitModel.urlString = bannerDict[@"ad_img"];
                    [bannersM addObject:unitModel.urlString];
                    [bannerModelsM addObject:unitModel];
                }
                self.banners = bannersM;
                self.bannerModels = bannerModelsM;
            }
        }
    }
    
    NSArray *tutor = dataDict[@"tutor_curriculum"];
    if ([tutor isKindOfClass:[NSArray class]]) {
        if (tutor.count) {
            NSMutableArray *unitModelsM = [NSMutableArray new];
            for (NSInteger index = 0; index < tutor.count; index++) {
                NSDictionary *unitDict = tutor[index];
                ZLHomeModel *unitModel = [[ZLHomeModel alloc] init];
                unitModel.keyId = unitDict[@"id"];
                unitModel.keyTitle = unitDict[@"title"];
                unitModel.urlString = unitDict[@"img_one"];
                [unitModelsM addObject:unitModel];
            }
            
            //标识没有更多数据
            if (unitModelsM.count < self.size) {
                self.noMore = YES;
            }
            
            //根据情况整理数据
            if (!self.loadMore) {
                self.unitModelsM = unitModelsM;
            }else {
                [self.unitModelsM addObjectsFromArray:unitModelsM];
            }
            return;
        }
    }
    
    //标识没有更多数据
    if (self.loadMore) {
        self.noMore = YES;
    }
}

@end
