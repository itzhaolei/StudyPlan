//
//  ZLHomeTableHeader.m
//  Home
//
//  Created by zhaolei on 2018/11/8.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ZLHomeTableHeader.h"
#import <SDCycleScrollView.h>

@interface ZLHomeTableHeader ()<SDCycleScrollViewDelegate>

///轮播图
@property (nonatomic,weak) SDCycleScrollView *cycleScrollView;

@end

@implementation ZLHomeTableHeader

#pragma mark - Set
- (void)setUrls:(NSArray *)urls {
    _urls = urls;
    self.cycleScrollView.imageURLStringsGroup = urls;
}

#pragma mark - Lazy
- (SDCycleScrollView *)cycleScrollView{//16：9
    if (!_cycleScrollView) {
        SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.width / 16 * 9) delegate:self placeholderImage:nil];
        cycleScrollView.autoScrollTimeInterval = 10;
        cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
        cycleScrollView.titlesGroup = @[@"测试1测试1测试1测试1测试1测试1",@"测试2测试2测试2测试2测试2",@"测试3测试3测试3测试3测试3"];
        [self addSubview:cycleScrollView];
        _cycleScrollView = cycleScrollView;
    }
    return _cycleScrollView;
}

#pragma mark - SDCycleScrollViewDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    if (self.click) {
        self.click(index);
    }
}

@end
