//
//  ZLHomeTableHeader.h
//  Home
//
//  Created by zhaolei on 2018/11/8.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZLHomeTableHeader : UIView

///轮播数据源
@property (nonatomic,strong) NSArray *urls;
///点击事件
@property (nonatomic,copy) void (^click)(NSInteger index);

@end

NS_ASSUME_NONNULL_END
