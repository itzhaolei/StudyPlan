//
//  ZLHomeView.h
//  Home
//
//  Created by zhaolei on 2018/11/6.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLHomeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZLHomeView : UIView

///强引用模型
@property (nonatomic,strong) ZLHomeModel *infoModel;

@end

NS_ASSUME_NONNULL_END
