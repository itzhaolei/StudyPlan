//
//  ZLHomeView.m
//  Home
//
//  Created by zhaolei on 2018/11/6.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ZLHomeView.h"
#import "ZLHomeViewCell.h"
#import <UIImageView+AFNetworking.h>
#import "ZLHomeTableHeader.h"
#import <MJRefresh.h>

@interface ZLHomeView ()<UITableViewDataSource,UITableViewDelegate>

///滑动视图
@property (nonatomic,weak) UITableView *tableView;
///表头
@property (nonatomic,strong) ZLHomeTableHeader *tableHeader;

@end

@implementation ZLHomeView

#pragma mark - Set
- (void)setInfoModel:(ZLHomeModel *)infoModel {
    _infoModel = infoModel;
    [self registerAction];
}

#pragma mark - Lazy
- (UITableView *)tableView {
    if (!_tableView) {
        UITableView *tableView = [[UITableView alloc] initWithFrame:self.bounds style:(UITableViewStylePlain)];
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.rowHeight = 150.0;
        tableView.separatorStyle = NO;
        
        __weak typeof(self)weakSelf = self;
        
        tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{//下拉刷新
            if (weakSelf.infoModel.load) {
                weakSelf.infoModel.noMore = NO;
                weakSelf.infoModel.page = 1;
                weakSelf.infoModel.loadMore = NO;
                weakSelf.infoModel.load();
            }
        }];
        tableView.mj_footer=[MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{//上拉加载
            if (weakSelf.infoModel.load) {
                weakSelf.infoModel.page += 1;
                weakSelf.infoModel.loadMore = YES;
                weakSelf.infoModel.load();
            }
        }];
        tableView.mj_footer.hidden = YES;
        
        //ios11 适配
        if (@available(iOS 11.0, *)) {
            tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            tableView.scrollIndicatorInsets = tableView.contentInset;
            tableView.estimatedRowHeight = 0;
            tableView.estimatedSectionHeaderHeight = 0;
            tableView.estimatedSectionFooterHeight = 0;
        }
        
        [self addSubview:tableView];
        _tableView = tableView;
    }
    return _tableView;
}
- (ZLHomeTableHeader *)tableHeader {
    if (!_tableHeader) {
        _tableHeader = [[ZLHomeTableHeader alloc] initWithFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.width / 16 * 9 + 15.0)];
        _tableHeader.backgroundColor = UIColor.whiteColor;
        __weak typeof(self)weakSelf = self;
        _tableHeader.click = ^(NSInteger index) {
            if (weakSelf.infoModel.click) {
                weakSelf.infoModel.click(index);
            }
        };
    }
    return _tableHeader;
}

#pragma mark - Action
//在得到模型的同时，注册模型的事件
- (void)registerAction {
    
    __weak typeof(self)weakSelf = self;
    
    //处理结果
    self.infoModel.result = ^{
        if (weakSelf.infoModel.bannerModels) {
            weakSelf.tableView.tableHeaderView = weakSelf.tableHeader;
            weakSelf.tableHeader.urls = weakSelf.infoModel.banners;
        }else {
            weakSelf.tableView.tableHeaderView = nil;
        }
        
        [weakSelf.tableView reloadData];

        if (weakSelf.infoModel.unitModelsM.count && !weakSelf.infoModel.noMore) {
            weakSelf.tableView.mj_footer.hidden = NO;
        }
        
        if (weakSelf.infoModel.loadMore) {
            [weakSelf.tableView.mj_footer endRefreshing];
        }else {
            [weakSelf.tableView.mj_header endRefreshing];
        }

        if (weakSelf.infoModel.noMore) {
            [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
        }else {
            [weakSelf.tableView.mj_footer resetNoMoreData];
        }
        
    };
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ceil(self.infoModel.unitModelsM.count / 2.0);
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZLHomeViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZLHomeViewCell"];
    if (!cell) {
        cell = [[ZLHomeViewCell alloc] initWithStyle:(UITableViewCellStyleSubtitle) reuseIdentifier:@"ZLHomeViewCell"];
        cell.selectionStyle = NO;
    }
    
    //注册cell单元视图的点击事件
    cell.click = ^(NSInteger index) {
        NSLog(@"%ld",indexPath.row * 2 + index);
    };
    
    for (NSInteger index = 0; index < cell.unitView.subviews.count; index++) {
        ZLHomeViewUnitView *unitView = cell.unitView.subviews[index];
        unitView.hidden = YES;
        NSInteger currentIndex = indexPath.row * 2 + index;
        if (currentIndex < self.infoModel.unitModelsM.count) {
            ZLHomeModel *unitModel = self.infoModel.unitModelsM[currentIndex];
            unitView.titleLabel.text = unitModel.keyTitle;
            unitView.hidden = NO;
            [unitView.iconImageView setImageWithURL:[NSURL URLWithString:unitModel.urlString]];
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate


@end
