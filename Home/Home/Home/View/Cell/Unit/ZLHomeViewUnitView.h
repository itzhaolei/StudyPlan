//
//  ZLHomeViewUnitView.h
//  Home
//
//  Created by zhaolei on 2018/11/6.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZLHomeViewUnitView : UIView

///封面
@property (nonatomic,weak) UIImageView *iconImageView;
///标题
@property (nonatomic,weak) UILabel *titleLabel;
///点击事件
@property (nonatomic,copy) void (^click)(ZLHomeViewUnitView *unitView);

@end

NS_ASSUME_NONNULL_END
