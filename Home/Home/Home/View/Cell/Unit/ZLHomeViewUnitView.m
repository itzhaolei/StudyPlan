//
//  ZLHomeViewUnitView.m
//  Home
//
//  Created by zhaolei on 2018/11/6.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ZLHomeViewUnitView.h"

@implementation ZLHomeViewUnitView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = UIColor.lightGrayColor;
        self.layer.cornerRadius = 6.0;
        self.layer.masksToBounds = YES;
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)]];
    }
    return self;
}

#pragma mark - Lazy
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10.0, self.frame.size.height - 30.0, self.frame.size.width - 20.0, 30.0)];
        label.font = [UIFont systemFontOfSize:15.0];
        [self addSubview:label];
        _titleLabel = label;
    }
    return _titleLabel;
}
- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 30.0)];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [self addSubview:imageView];
        _iconImageView = imageView;
    }
    return _iconImageView;
}

#pragma mark - Action
- (void)tapAction {
    if (self.click) {
        self.click(self);
    }
}

@end
