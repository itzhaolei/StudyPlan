//
//  ZLHomeViewCell.m
//  Home
//
//  Created by zhaolei on 2018/11/6.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ZLHomeViewCell.h"

@implementation ZLHomeViewCell

#pragma mark - Lazy
- (UIView *)unitView {
    if (!_unitView) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, 150.0)];
        
        CGSize screenSize = UIScreen.mainScreen.bounds.size;
        CGFloat width = (screenSize.width - 40.0) / 2;
        for (NSInteger index = 0; index < 2; index++) {
            ZLHomeViewUnitView *subView = [[ZLHomeViewUnitView alloc] initWithFrame:CGRectMake(15.0 + (width + 10.0) * index, 0, width, CGRectGetHeight(view.frame) - 10.0)];
            
            __weak typeof(self)weakSelf = self;
            subView.click = ^(ZLHomeViewUnitView * _Nonnull unitView) {
                NSInteger index = [weakSelf.unitView.subviews indexOfObject:unitView];
                if (weakSelf.click) {
                    weakSelf.click(index);
                }
            };
            
            [view addSubview:subView];
        }
        
        [self.contentView addSubview:view];
        _unitView = view;
    }
    return _unitView;
}

@end
