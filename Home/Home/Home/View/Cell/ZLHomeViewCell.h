//
//  ZLHomeViewCell.h
//  Home
//
//  Created by zhaolei on 2018/11/6.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLHomeViewUnitView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZLHomeViewCell : UITableViewCell

///单元视图
@property (nonatomic,weak) UIView *unitView;
///点击事件
@property (nonatomic,copy) void (^click)(NSInteger index);

@end

NS_ASSUME_NONNULL_END
