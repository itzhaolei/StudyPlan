//
//  ZLHomeViewController.m
//  Home
//
//  Created by zhaolei on 2018/11/6.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ZLHomeViewController.h"
#import "ZLHomeView.h"

@interface ZLHomeViewController ()

///主视图
@property (nonatomic,weak) ZLHomeView *mainView;

@end

@implementation ZLHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self mainView];
    self.mainView.infoModel = [ZLHomeModel loadModel];
    if (self.mainView.infoModel.result) {
        self.mainView.infoModel.result();
    }
    [self registerAction];
}

#pragma mark - Action
- (void)registerAction {
    __weak typeof(self)weakSelf = self;
    
    self.mainView.infoModel.click = ^(NSInteger index) {
        NSLog(@"%@",weakSelf.mainView.infoModel.bannerModels[index].keyId);
    };
}

#pragma mark - Lazy
- (ZLHomeView *)mainView {
    if (!_mainView) {
        ZLHomeView *mainView = [[ZLHomeView alloc] initWithFrame:UIScreen.mainScreen.bounds];
        [self.view addSubview:mainView];
        _mainView = mainView;
    }
    return _mainView;
}

@end
