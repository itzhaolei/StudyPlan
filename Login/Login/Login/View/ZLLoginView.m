//
//  ZLLoginView.m
//  Login
//
//  Created by zhaolei on 2018/11/4.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ZLLoginView.h"

@interface ZLLoginView ()

///手机号
@property (nonatomic,strong) NSString *phone;
///验证码
@property (nonatomic,strong) NSString *code;

@end

@implementation ZLLoginView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.phone = @"13586350997";
        self.code = @"106564";
        
        for (NSInteger index = 0; index < 2; index++) {
            UIButton *sender = [[UIButton alloc] initWithFrame:CGRectMake(100.0 * index, 50.0, 80.0, 40.0)];
            sender.backgroundColor = !index ? UIColor.redColor : UIColor.cyanColor;
            [sender setTitle:!index ? @"发送验证码" : @"登录" forState:UIControlStateNormal];
            sender.layer.cornerRadius = 6.0;
            sender.layer.masksToBounds = YES;
            sender.titleLabel.font = [UIFont systemFontOfSize:13.0];
            if (!index) {
                [sender addTarget:self action:@selector(sendCode) forControlEvents:UIControlEventTouchUpInside];
            }else {
                [sender addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
            }
            
            [self addSubview:sender];
        }
    }
    return self;
}

#pragma mark - Set
- (void)setInfoModel:(ZLLoginModel *)infoModel {
    _infoModel = infoModel;
    [self registerAction];
}

#pragma mark - Action
//在得到模型的同时，注册模型的事件
- (void)registerAction {
    
    __weak typeof(self)weakSelf = self;
    
    //验证码结果
    self.infoModel.sendCodeResults = ^{
        //启动定时器、进行倒计时
        
    };
    
    //登录结果
    self.infoModel.sendLoginResults = ^{
        //登录成功、给出提示
        NSLog(@"登录成功");
    };
}
- (void)sendCode {
    self.infoModel.phone = self.phone;
    if (self.infoModel.sendCode) {
        self.infoModel.sendCode();
    }
}
- (void)login {
    self.infoModel.code = self.code;
    if (self.infoModel.sendLogin) {
        self.infoModel.sendLogin();
    }
}

@end
