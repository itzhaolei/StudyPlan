//
//  ZLLoginView.h
//  Login
//
//  Created by zhaolei on 2018/11/4.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLLoginModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZLLoginView : UIView

///强引用模型
@property (nonatomic,strong) ZLLoginModel *infoModel;

@end

NS_ASSUME_NONNULL_END
