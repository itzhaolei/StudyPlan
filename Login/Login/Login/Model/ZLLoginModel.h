//
//  ZLLoginModel.h
//  Login
//
//  Created by zhaolei on 2018/11/4.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZLLoginModel : NSObject

///电话号码
@property (nonatomic,strong) NSString *phone;
///验证码
@property (nonatomic,strong) NSString *code;
///令牌
@property (nonatomic,strong) NSString *token;


///请求验证码
@property (nonatomic,copy) void (^sendCode)(void);
///验证码结果
@property (nonatomic,copy) void (^sendCodeResults)(void);
///请求登录
@property (nonatomic,copy) void (^sendLogin)(void);
///登录结果
@property (nonatomic,copy) void (^sendLoginResults)(void);


///加载模型
+ (instancetype)loadModel;

@end

NS_ASSUME_NONNULL_END
