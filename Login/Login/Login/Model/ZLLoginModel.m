//
//  ZLLoginModel.m
//  Login
//
//  Created by zhaolei on 2018/11/4.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ZLLoginModel.h"
#import "ZLHTTPSessionManager.h"

@implementation ZLLoginModel

+ (instancetype)loadModel {
    ZLLoginModel *infoModel = [self new];
    
    __weak typeof(infoModel)weakModel = infoModel;
    
    ///发送验证码
    infoModel.sendCode = ^{
        [weakModel sendCodeAction];
    };
    
    ///发送登录
    infoModel.sendLogin = ^{
        [weakModel sendLoginAction];
    };
    
    return infoModel;
}

///发送验证码事件
- (void)sendCodeAction {
    NSMutableDictionary *dictM = [NSMutableDictionary new];
    dictM[@"mobile"] = self.phone;
    dictM[@"type"] = @(1);
    __weak typeof(self)weakSelf = self;
    [ZLHTTPSessionManager requestDataWithUrlPath:@"https://api.91tumi.com/b99463d58a5c8372e6adbdca867428961641cb51.go" Params:dictM POST:YES ModelArray:nil HttpHeader:NO CachePolicy:NO Results:^(ZLSessionManagerErrorState sessionErrorState, id responseObject) {
        if (!sessionErrorState) {
            if (![responseObject[@"code"] integerValue]) {
                weakSelf.token = responseObject[@"data"];
                if (weakSelf.sendCodeResults) {
                    weakSelf.sendCodeResults();
                }
                return;
            }
            //给用户提示（后台返回的字符串）
            return;
        }
        //网络出错
    }];
}

///发送登录事件
- (void)sendLoginAction {
    NSMutableDictionary *dictM = [NSMutableDictionary new];
    dictM[@"code"] = self.code;
    dictM[@"mobile"] = self.phone;
    dictM[@"token"] = self.token;
    dictM[@"type"] = @1;
    __weak typeof(self)weakSelf = self;
    [ZLHTTPSessionManager requestDataWithUrlPath:@"https://api.91tumi.com/applogin.go" Params:dictM POST:YES ModelArray:nil HttpHeader:NO CachePolicy:NO Results:^(ZLSessionManagerErrorState sessionErrorState, id responseObject) {
        if (!sessionErrorState) {
            if (![responseObject[@"code"] integerValue]) {
                if (weakSelf.sendLoginResults) {
                    weakSelf.sendLoginResults();
                }
                return;
            }
            //登录失败、给用户提示（后台返回的字符串）
            return;
        }
        //网络出错
    }];
}

@end
