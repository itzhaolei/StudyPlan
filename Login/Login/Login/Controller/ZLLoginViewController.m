//
//  ZLLoginViewController.m
//  Login
//
//  Created by zhaolei on 2018/11/4.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ZLLoginViewController.h"
#import "ZLLoginView.h"

@interface ZLLoginViewController ()

///主视图
@property (nonatomic,weak) ZLLoginView *mainView;

@end

@implementation ZLLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self mainView];
    self.mainView.infoModel = [ZLLoginModel loadModel];
}

#pragma mark - Lazy
- (ZLLoginView *)mainView {
    if (!_mainView) {
        ZLLoginView *mainView = [[ZLLoginView alloc] initWithFrame:UIScreen.mainScreen.bounds];
        [self.view addSubview:mainView];
        _mainView = mainView;
    }
    return _mainView;
}

@end
