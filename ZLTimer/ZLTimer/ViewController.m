//
//  ViewController.m
//  ZLTimer
//
//  Created by zhaolei on 2018/10/29.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

///
@property (nonatomic,strong) NSTimer *timer;
///
@property (nonatomic,unsafe_unretained) int number;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.number = 99999;
    __weak typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        @autoreleasepool {
            weakSelf.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
            [weakSelf.timer fire];
            [[NSRunLoop currentRunLoop] addTimer:weakSelf.timer forMode:NSDefaultRunLoopMode];
            [[NSRunLoop currentRunLoop] run];
        }
    });
}

#pragma mark - Action
- (void)timerAction {
//    NSLog(@"----%@",[NSThread currentThread]);
    NSString *string = nil;
    
    self.number--;
    int xiaoshi = floor(self.number / 60.0 / 60.0);
    int zongmiao = self.number - xiaoshi * 60 * 60;
    int fenzhong = floor(zongmiao / 60.0);
    int miao = zongmiao - fenzhong * 60;
    
    string = [NSString stringWithFormat:@"%d时%d分%d秒",xiaoshi,fenzhong,miao];
    NSLog(@"%@",string);
    
}

@end
