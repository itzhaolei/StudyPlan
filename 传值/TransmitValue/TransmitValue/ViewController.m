//
//  ViewController.m
//  TransmitValue
//
//  Created by zhaolei on 2018/10/26.
//  Copyright © 2018年 赵磊. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

///主视图
@property (nonatomic,weak) UIView *mainView;
///标题
@property (nonatomic,weak) UILabel *titleLabel;

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self mainView];
}

#pragma mark - Lazy
- (UIView *)mainView {
    if (!_mainView) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, UIScreen.mainScreen.bounds.size.height * 0.5, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height * 0.5)];
        view.backgroundColor = [UIColor colorWithRed:arc4random()%255 / 255.0 green:arc4random()%255 / 255.0 blue:arc4random()%255 / 255.0 alpha:1.0];
        
        [self.view addSubview:view];
        _mainView = view;
    }
    return _mainView;
}



@end
